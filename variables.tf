variable "parent_folder_id" {
  type        = string
  description = ""
  default     = "nonprod-279407"
}
variable "project_id" {
  type        = string
  description = "project you can assign"
  default     = ""
}
variable "machine_type" {
  type        = string
  description = ""
  default     = "e2-standard-4"
}
variable "initial_node_count" {
  type        = string
  description = "number of nodes you can have"
  #default = "2"
}
variable "name" {
  type        = string
  description = "name of the cluster"
  #default = "mrRoger"
}
variable "node_count" {
  type        = string
  description = "number of nodes"
  #default = "1"
}